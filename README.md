# ci-templates

## Usage

### Basic

```yaml
include:
  - project: 'ruanbekker/ci-templates'
    ref: main
    file: 'basic/echo.yml'

example:
  extends: .hello
  variables:
    WORD: "example"
```

### Arkade

To run `arkade --help`:

```yaml
include:
  - project: 'ruanbekker/ci-templates'
    ref: main
    file:
      - 'arkade/main.yml'
```

Template:

```
-> arkade/main.yml
   -> arkade/jobs.yml (steps to run)
      -> arkade/tasks.yml (definition of how to run them)
```
